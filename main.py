import pygame, sys
from pd_lib import settings, animation, image

def get_screen_info():
	s_info = pygame.display.Info()
	s_width = s_info.current_w
	s_height = s_info.current_h
	# Ekran Genişliği 16 ya 9 mu anlayalım
	if s_width/s_height == 16/9:
		settings.SCALE_FACTOR = s_width/1920
		settings.SCREEN_WIDTH = s_width
		settings.SCREEN_HEIGHT = s_height
	else:
		x_scale_factor = s_width / 1920
		y_scale_factor = s_height / 1080
		if x_scale_factor >= y_scale_factor:
			settings.SCALE_FACTOR = y_scale_factor
			settings.SCREEN_WIDTH = int(1920*settings.SCALE_FACTOR)
			settings.SCREEN_HEIGHT = s_height
		else:
			settings.SCALE_FACTOR = x_scale_factor
			settings.SCREEN_WIDTH = s_width
			settings.SCREEN_HEIGHT = int(1080*settings.SCALE_FACTOR)

def read_settings():
	pass


def write_settings():
	pass

#Ayarları okuyalım
read_settings()
pygame.init()
get_screen_info()
FPSCLOCK = pygame.time.Clock()
settings.SCREEN = pygame.display.set_mode((settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT))

print(settings.SCREEN_WIDTH, settings.SCREEN_HEIGHT)
print(settings.SCALE_FACTOR)

a_dict = {"name":"run",
		"frames":[image.Image("./assets/Attack__000.png",[25,25]),image.Image("./assets/Attack__001.png",[25,25]),image.Image("./assets/Attack__002.png",[25,25]),
					image.Image("./assets/Attack__003.png",[25,25]),image.Image("./assets/Attack__004.png",[25,25]),image.Image("./assets/Attack__005.png",[25,25])],
		"frame_back" : False,
		"scale":False,#[2,5,True,True],
		"rotate":False,#[30,11,True,False],
		"loop":True,
		"animation_speed":60,
		"sound":False}
d_animasyon = animation.Animation(None)
d_animasyon.add_animation(a_dict)
d_animasyon.set_active_animation("run")

while True:
	#tuş eventleri
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			pygame.quit()
			#Ayarları Yazalım
			write_settings()
			sys.exit()
		elif event.type == pygame.MOUSEBUTTONDOWN:
			pass
			#print(pygame.mouse.get_pos())

	d_animasyon.update(FPSCLOCK.get_rawtime())
	d_animasyon.draw()
	#update()
	#draw()
	pygame.display.flip()
	FPSCLOCK.tick(settings.FPS)


