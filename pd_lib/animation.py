from pd_lib import settings
import pygame

class Animation():
	def __init__(self, parent):
		self.parent = parent
		self.animations = {}
		self.active_animation = None
		self.last_update_time = 0
		self.time = 0
		self.first_animation = 0

	def add_animation(self,animation_dict):
		animation_name = animation_dict["name"]
		del animation_dict["name"]
		self.animations[animation_name] = animation_dict
		if self.animations[animation_name]["scale"]:
			self.animations[animation_name]["scale_over"] = 0
		if self.animations[animation_name]["rotate"]:
			self.animations[animation_name]["rotate_over"] = 0
		self.animations[animation_name]["frame_flow"] = "up"
		self.animations[animation_name]["scale_flow"] = "up"
		self.animations[animation_name]["rotate_flow"] = "up"
		self.animations[animation_name]["active_frame"] = 0
		#self.animations[animation_name]["rotate_loop_num"] = 0
		#self.animations[animation_name]["scale_loop_num"] = 0
		#self.animations[animation_name]["frame_loop_num"] = 0


	def set_active_animation(self,animation_name):
		self.active_animation = animation_name
		for animation_name in self.animations.keys():
			self.animations[animation_name]["active_frame"] = 0

	def update(self,time,position=False):
		if position:
			for animation in self.animations.keys():
				self.animations[animation]["position"] = position
		# Animasyon sonraki frame geçmesini sağlıyoruz eğer sonraki frame yoksa başa dönüyor
		self.time += time
		if (self.time - self.last_update_time) > self.animations[self.active_animation]["animation_speed"]:
			if self.animations[self.active_animation]["loop"]:
				if self.animations[self.active_animation]["frame_back"]:
					if self.animations[self.active_animation]["active_frame"] == 0:
						self.animations[self.active_animation]["frame_flow"] = "up"
					elif self.animations[self.active_animation]["active_frame"] == len(self.animations[self.active_animation]["frames"]) - 1:
						self.animations[self.active_animation]["frame_flow"] = "down"
				else:
					if self.animations[self.active_animation]["active_frame"] == len(self.animations[self.active_animation]["frames"]) - 1:
						self.animations[self.active_animation]["active_frame"] = -1
			else:
				if self.animations[self.active_animation]["frame_back"]:
					if self.animations[self.active_animation]["active_frame"] == 0 and self.first_animation:
						self.animations[self.active_animation]["frame_flow"] = "const"
					elif self.animations[self.active_animation]["active_frame"] == len(self.animations[self.active_animation]["frames"]) - 1:
						self.animations[self.active_animation]["frame_flow"] = "down"
				else:
					if self.animations[self.active_animation]["active_frame"] == len(self.animations[self.active_animation]["frames"]) - 1:
						self.animations[self.active_animation]["frame_flow"] = "const"
			if self.animations[self.active_animation]["frame_flow"] == "up":
				self.animations[self.active_animation]["active_frame"] += 1
			elif self.animations[self.active_animation]["frame_flow"] == "down":
				self.animations[self.active_animation]["active_frame"] -= 1

			if self.animations[self.active_animation]["scale"]:
				if self.animations[self.active_animation]["scale"][3]:
					if self.animations[self.active_animation]["scale"][2]:
						if self.animations[self.active_animation]["scale_over"] == 0:
							self.animations[self.active_animation]["scale_flow"] = "up"
						elif self.animations[self.active_animation]["scale_over"] == self.animations[self.active_animation]["scale"][1]:
							self.animations[self.active_animation]["scale_flow"] = "down"
					else:
						if self.animations[self.active_animation]["scale_over"] == self.animations[self.active_animation]["scale"][1]:
							self.animations[self.active_animation]["scale_over"] = -1
				else:
					if self.animations[self.active_animation]["scale"][2]:
						if self.animations[self.active_animation]["scale_over"] == 0 and self.first_animation:
							self.animations[self.active_animation]["scale_flow"] = "const"
						elif self.animations[self.active_animation]["scale_over"] == self.animations[self.active_animation]["scale"][1]:
							self.animations[self.active_animation]["scale_flow"] = "down"
					else:
						if self.animations[self.active_animation]["scale_over"] == self.animations[self.active_animation]["scale"][1]:
							self.animations[self.active_animation]["scale_flow"] = "const"
				if self.animations[self.active_animation]["scale_flow"] == "up":
					self.animations[self.active_animation]["scale_over"] += 1
				elif self.animations[self.active_animation]["scale_flow"] == "down":
					self.animations[self.active_animation]["scale_over"] -= 1

			if self.animations[self.active_animation]["rotate"]:
				if self.animations[self.active_animation]["rotate"][3]:
					if self.animations[self.active_animation]["rotate"][2]:
						if self.animations[self.active_animation]["rotate_over"] == 0:
							self.animations[self.active_animation]["rotate_flow"] = "up"
						elif self.animations[self.active_animation]["rotate_over"] == self.animations[self.active_animation]["rotate"][1]:
							self.animations[self.active_animation]["rotate_flow"] = "down"
					else:
						if self.animations[self.active_animation]["rotate_over"] == self.animations[self.active_animation]["rotate"][1]:
							self.animations[self.active_animation]["rotate_over"] = -1
				else:
					if self.animations[self.active_animation]["rotate"][2]:
						if self.animations[self.active_animation]["rotate_over"] == 0 and self.first_animation:
							self.animations[self.active_animation]["rotate_flow"] = "const"
						elif self.animations[self.active_animation]["rotate_over"] == self.animations[self.active_animation]["rotate"][1]:
							self.animations[self.active_animation]["rotate_flow"] = "down"
					else:
						if self.animations[self.active_animation]["rotate_over"] == self.animations[self.active_animation]["rotate"][1]:
							self.animations[self.active_animation]["rotate_flow"] = "const"
				if self.animations[self.active_animation]["rotate_flow"] == "up":
					self.animations[self.active_animation]["rotate_over"] += 1
				elif self.animations[self.active_animation]["rotate_flow"] == "down":
					self.animations[self.active_animation]["rotate_over"] -= 1

			self.first_animation = 1
			self.last_update_time = self.time

	def draw(self):
		settings.SCREEN.fill((0, 0, 0))
		active_frame = self.animations[self.active_animation]["active_frame"]
		frames = self.animations[self.active_animation]["frames"]
		frame = frames[active_frame]
		if self.animations[self.active_animation]["scale"]:
			scale = self.animations[self.active_animation]["scale"][0]
			scale_over = self.animations[self.active_animation]["scale_over"]
			scale_fac = (scale - 1) / self.animations[self.active_animation]["scale"][1]
			scale_fac = scale_fac * scale_over
			scale_fac += 1
			frame.set_scale(scale_fac)
		if self.animations[self.active_animation]["rotate"]:
			rotate = self.animations[self.active_animation]["rotate"][0]
			rotate_over = self.animations[self.active_animation]["rotate_over"]
			frame.set_rotate(rotate_over* rotate)
		frame.draw()
