from pd_lib import settings
import pygame

class Image():
	def __init__(self,path,position):
		self.first_img = pygame.image.load(path)
		self.position = position
		self.scale_fac = 1
		self.rotate_ang = 10

	def set_position(self,position):
		self.position = position

	def draw(self):
		img = self.scale_img(self.first_img)
		img = self.rotate_img(img)
		settings.SCREEN.blit(img,self.position)

	def set_scale(self,scale_fac):
		self.scale_fac = scale_fac

	def set_rotate(self,rotate_ang):
		self.rotate_ang = rotate_ang

	def scale_img(self,img):
		img_size = self.get_img_size(img)
		img = pygame.transform.scale(img,(int(img_size[0]*self.scale_fac*settings.SCALE_FACTOR),int(img_size[1]*self.scale_fac*settings.SCALE_FACTOR)))
		return img

	def rotate_img(self,img):
		img = pygame.transform.rotate(img, self.rotate_ang)
		return img

	def get_img_size(self,img):
		return img.get_rect().size
