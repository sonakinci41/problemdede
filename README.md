# PROBLEM DEDE YOL HARİTASI
## GENEL AKIŞ

![](./assets/prgs.svg)

## RENKLER 

`rgba(33,37,41,1)`  `#212529`
`rgba(52,58,64,1)`  `#343a40`
`rgba(73,80,87,1)`  `#495057`
`rgba(108,117,125,1)`  `#6c757d`
`rgba(173,181,189,1)`  `#adb5bd`
`rgba(206,212,218,1)`  `#ced4da`
`rgba(222,226,230,1)`  `#dee2e6`
`rgba(233,236,239,1)`  `#e9ecef`
`rgba(248,249,250,1)`  `#f8f9fa`
`rgba(255,255,255,1)`  `#ffffff`
`rgba(13,110,253,1)`  `#0d6efd`
`rgba(168,203,254,1)`  `#a8cbfe`
`rgba(129,180,254,1)`  `#81b4fe`
`rgba(90,156,254,1)`  `#5a9cfe`
`rgba(52,133,253,1)`  `#3485fd`
`rgba(11,92,213,1)`  `#0b5cd5`
`rgba(9,75,172,1)`  `#094bac`
`rgba(7,57,132,1)`  `#073984`
`rgba(5,40,91,1)`  `#05285b`
`rgba(102,16,242,1)`  `#6610f2`
`rgba(200,169,250,1)`  `#c8a9fa`
`rgba(175,131,248,1)`  `#af83f8`
`rgba(151,92,246,1)`  `#975cf6`
`rgba(126,54,244,1)`  `#7e36f4`
`rgba(86,13,203,1)`  `#560dcb`
`rgba(69,11,165,1)`  `#450ba5`
`rgba(53,8,126,1)`  `#35087e`
`rgba(37,6,87,1)`  `#250657`
`rgba(111,66,193,1)`  `#6f42c1`
`rgba(203,187,233,1)`  `#cbbbe9`
`rgba(180,157,223,1)`  `#b49ddf`
`rgba(157,126,213,1)`  `#9d7ed5`
`rgba(134,96,203,1)`  `#8660cb`
`rgba(93,55,162,1)`  `#5d37a2`
`rgba(75,45,131,1)`  `#4b2d83`
`rgba(58,34,100,1)`  `#3a2264`
`rgba(40,24,69,1)`  `#281845`
`rgba(214,51,132,1)`  `#d63384`
`rgba(240,182,211,1)`  `#f0b6d3`
`rgba(234,149,191,1)`  `#ea95bf`
`rgba(227,116,171,1)`  `#e374ab`
`rgba(221,84,152,1)`  `#dd5498`
`rgba(180,43,111,1)`  `#b42b6f`
`rgba(146,35,90,1)`  `#92235a`
`rgba(111,27,69,1)`  `#6f1b45`
`rgba(77,18,48,1)`  `#4d1230`
`rgba(220,53,69,1)`  `#dc3545`
`rgba(242,182,188,1)`  `#f2b6bc`
`rgba(237,150,158,1)`  `#ed969e`
`rgba(231,118,129,1)`  `#e77681`
`rgba(226,85,99,1)`  `#e25563`
`rgba(185,45,58,1)`  `#b92d3a`
`rgba(150,36,47,1)`  `#96242f`
`rgba(114,28,36,1)`  `#721c24`
`rgba(79,19,25,1)`  `#4f1319`
`rgba(253,126,20,1)`  `#fd7e14`
`rgba(254,209,170,1)`  `#fed1aa`
`rgba(254,188,133,1)`  `#febc85`
`rgba(254,167,95,1)`  `#fea75f`
`rgba(253,147,58,1)`  `#fd933a`
`rgba(213,106,17,1)`  `#d56a11`
`rgba(172,86,14,1)` `#ac560e`
`rgba(132,66,10,1)` `#84420a`
`rgba(91,45,7,1)` `#5b2d07`
`rgba(255,193,7,1)` `#ffc107`
`rgba(255,233,166,1)`  `#ffe9a6`
`rgba(255,223,126,1)`  `#ffdf7e`
`rgba(255,213,86,1)`  `#ffd556`
`rgba(255,203,47,1)`  `#ffcb2f`
`rgba(214,162,6,1)`  `#d6a206`
`rgba(173,131,5,1)`  `#ad8305`
`rgba(133,100,4,1)`  `#856404`
`rgba(92,69,3,1)`  `#5c4503`
`rgba(40,167,69,1)`  `#28a745`
`rgba(178,223,188,1)`  `#b2dfbc`
`rgba(143,209,158,1)`  `#8fd19e`
`rgba(109,195,129,1)`  `#6dc381`
`rgba(74,181,99,1)`  `#4ab563`
`rgba(34,140,58,1)`  `#228c3a`
`rgba(27,114,47,1)`  `#1b722f`
`rgba(21,87,36,1)`  `#155724`
`rgba(14,60,25,1)`  `#0e3c19`
`rgba(32,201,151,1)`  `#20c997`
`rgba(175,236,218,1)`  `#afecda`
`rgba(139,227,201,1)`  `#8be3c9`
`rgba(103,218,184,1)`  `#67dab8`
`rgba(68,210,168,1)`  `#44d2a8`
`rgba(27,169,127,1)`  `#1ba97f`
`rgba(22,137,103,1)`  `#168967`
`rgba(17,105,79,1)`  `#11694f`
`rgba(12,72,54,1)`  `#0c4836`
`rgba(23,162,184,1)`  `#17a2b8`
`rgba(171,222,229,1)`  `#abdee5`
`rgba(134,207,218,1)`  `#86cfda`
`rgba(97,192,207,1)`  `#61c0cf`
`rgba(60 177,195,1)`  `#3cb1c3`
`rgba(19,136,155,1)`  `#13889b`
`rgba(16,110,125,1)`  `#106e7d`
`rgba(12,84,96,1)`  `#0c5460`
`rgba(8,58,66,1)`  `#083a42`


## SINIFLAR

### Resim Sınıfı
Her türlü resim sınıftan oluşturulacak

**Draw** : Resimi önceden atanmış posizyona çizer.

- Paremere Yok

**Set_Position** : Pozisyonu günceller.

- Paremere list in float

**Set_Scale** : Scale değerini günceller

- Parametre float

**Set_Rotate** : Rotate değerini günceller

- Parametre int 0-360

```python

resim = image.Image("./assets/Attack__000.png",[25,25])
resim.draw()

```

### Animasyon Sınıfı

Her türlü animasyon sınıftan oluşturulacak. Animasyon resimler arası geçiş olabileceği gibi resmi büyültme-küçültme dönrdürmede olabilir.

**Update** : Resim günceller, Resim Scale günceller, Resim Konum günceller, Resim Rotate günceller

- Parametreler (position or False, time)

**Draw** : Güncellenmiş frame çizer

- Parametre Yok

**Add_Animation** :

Animasyon eklenir bir animasyon sınıfı birden fazla animasyon alabilir.

Parametre (animasyon_dict)

- animasyon_dict öğeleri "name" : animation_name(str),
- "frames" : resimler(list in Resim Sınıfı),
- "scale" : scale degeri ve scale tekrar sayısı sondan onceki surekli donsun mu? son parametre False ise scale biter True ise tekrar başlangıç scale döner(float and int and bool and bool or False),
- "rotate" : rotate degeri ve rotate tekrar sayısı sondan onceki surekli donsun mu? son parametre False ise rotate biter True ise tekrar başlangıç scale döner (0-360 int and int and bool and bool or False),
- "loop" : animasyon sürekli dönecek mi? True-False (bool), 
- "animation_speed" : Animasyonun kaç frame de bir aktfi olacağını belirler frame (int)
- frame_back : Animasyon 123454321 şeklinde oynasın mı? True or False
- "sound" : Animasyon esnasında oynatılacak ses (Ses Sınıfı or False)

- Not Pozisyon silindi pozisyon işleminde resimin pozisyonu kriter alınacak.

**Set_Active_Animation** : Birden fazla animasyon olabileceği için şuan aktif olan animasyonu belirlemeye yarar

- Parametre (animation_name)

```python

# Örnek animasyon eklemesi
a_dict = {"name":"run",
		"frames":[image.Image("./assets/Attack__000.png",[25,25]),image.Image("./assets/Attack__001.png",[25,25]),image.Image("./assets/Attack__002.png",[25,25]),
					image.Image("./assets/Attack__003.png",[25,25]),image.Image("./assets/Attack__004.png",[25,25]),image.Image("./assets/Attack__005.png",[25,25])],
		"frame_back" : False,
		"scale":False,#[2,5,True,True],
		"rotate":False,#[30,11,True,False],
		"loop":True,
		"animation_speed":60,
		"sound":False}
d_animasyon = animation.Animation(None)
d_animasyon.add_animation(a_dict)
d_animasyon.set_active_animation("run")


```


### DragDrop Sınıfı

Nesnelerin tıklanmasıyla sürüklenmesi ve bırakılınca tekrar yerine dönmesi yada drop üzerinde gözükmesini sağlayan sınıf. Drag alanı drop alanı ve drag drop arası gezebilen objeleri tutar.

**Cursor_up** : Cursordan kaldırıldığında aktif drag_drop_obj varmı drop olacak mı yoksa drag alanına geri mi dönecek karar verilecek ve animasuon oynatılacak.

- Parametre Yok

**Cursor_down** : cursor bir drag_drop_obj si üzerinden mi? Eğer üzerindeyse drag_drop_obj sürüklenebilir hale getirilecek

- Parametre Yok

**Cursor_move** : aktif bir drag_drop_obj varsa objenin posizyonu güncellenecek

- Parametreler (position)

**Draw** : Güncellenmiş drag drop ve drag_drop_obj öğelerini çizer

- Paremereler Yok

**Add_drag_drop_objs** : Drag_drop öğeleri ekler

(list in dict) add_drag_drop_objs içindeki dict öğeleri

- "name": drag_drop objesinin adı,
- "animation" : Animasyon Sınıfı - animasyon sınıfında "drag" ve "drop" diye iki adet animasyon tanımlı olmalıdır.

**Add_drop_areas** : Drop alanları ekler

(drop_areas_list in dict) drop_areas_list içindeki dict öğeleri

- "name" : drop alanının adı (str),
- "drop_animation" drop esnasında oynatılacak animasyon (Animasyon Sınıfı),
- "hover_animation" : drop alanı üzerine gelindiğinde oynatılacak animasyon (Animasyon Sınıfı),
- "drag_drop_objs" = add_drag_drop_objs öğesine verilen listedeki öğelerin isimleri (list),
- "remove_drops" : Drop alanına bırakılan resim silinecek mi yoksa  çizilmeye devam edecek mi? True or False (bool)

**Add_drag_areas** : Drag alanları ekler

(drag_areas_list in dict) drag_areas_list içindeki dict öğeleri

- "name" : drag alanının adı (str),
- "drag_animation" : drag esnasında oynatılacak animasyon (Animasyon Sınıfı),
- "hover_animation" : drag alanı üzerine gelindiğinde oynatılacak animasyon (Animasyon Sınıfı),
- "drag_drop_objs" = Bu alanda drag olarak başlayacak objeler add_drag_drop_objs öğesine verilen listedeki öğelerin isimlerinden (list)

### Karakter Sınıfı

### LvL Sınıfı

### Ses Sınıfı

### Resim Sınıfı

### Fizik Sınıfı

### Button Sınıfı
